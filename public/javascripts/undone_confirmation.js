var UNDONE_BUTTON_SELECTOR = ".movement_undone_button";
var UNDONE_DIALOG_SELECTOR = "#dialog-confirm-undone";
var UNDONE_DIALOG_NOT_ZERO_HINT_SELECTOR = "#movement_undone_confirmation_zero_hint";
var current_undone_dialog_movement_timestamp = undefined;
var UNDONE_FORM_SELECTOR = "#hiddensetundoneform";
var UNDONE_FORM_TIME_INPUT_SELECTOR = "#hiddensetundoneform_timeinput";
$( document ).ready(function() {
    var dialog = $( UNDONE_DIALOG_SELECTOR ).dialog({
        autoOpen: false,
        resizable: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Set as undone": function() {
                $( UNDONE_FORM_TIME_INPUT_SELECTOR ).val( current_undone_dialog_movement_timestamp );
                $( UNDONE_FORM_SELECTOR ).submit();
                $( this ).dialog( "close" );
            },
            "Abort": function() {
                $( this ).dialog( "close" );
            }
        }
    });
    $( UNDONE_BUTTON_SELECTOR ).click(function() {
        current_undone_dialog_movement_timestamp = $( this).data( 'timestamp' );
        var currentValue = $( this).data( 'currentvalue' );
        if( currentValue == 0 ) {
            $( UNDONE_DIALOG_NOT_ZERO_HINT_SELECTOR ).show();
        } else {
            $( UNDONE_DIALOG_NOT_ZERO_HINT_SELECTOR ).hide();
        }

        dialog.dialog( "open" );
    });
});
