var DONE_BUTTON_SELECTOR = ".movement_done_button";
var DONE_DIALOG_SELECTOR = "#dialog-confirm-done";
var DONE_DIALOG_NOT_ZERO_HINT_SELECTOR = "#movement_done_confirmation_not_zero_hint";
var current_done_dialog_movement_timestamp = undefined;
var DONE_FORM_SELECTOR = "#hiddensetdoneform";
var DONE_FORM_TIME_INPUT_SELECTOR = "#hiddensetdoneform_timeinput";
$( document ).ready(function() {
    var dialog = $( DONE_DIALOG_SELECTOR ).dialog({
        autoOpen: false,
        resizable: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Set as done": function() {
                $( DONE_FORM_TIME_INPUT_SELECTOR ).val( current_done_dialog_movement_timestamp );
                $( DONE_FORM_SELECTOR ).submit();
                $( this ).dialog( "close" );
            },
            "Abort": function() {
                $( this ).dialog( "close" );
            }
        }
    });
    $( DONE_BUTTON_SELECTOR ).click(function() {
        current_done_dialog_movement_timestamp = $( this).data( 'timestamp' );
        var currentValue = $( this).data( 'currentvalue' );
        if( currentValue != 0 ) {
            $( DONE_DIALOG_NOT_ZERO_HINT_SELECTOR ).show();
        } else {
            $( DONE_DIALOG_NOT_ZERO_HINT_SELECTOR ).hide();
        }

        dialog.dialog( "open" );
    });
});
