create table Users (
     Name varchar(15) not null,
     Surname varchar(15) not null,
     Username varchar(15) not null,
     Password char(270) not null,
     ClearPassword varchar(15) not null,
     Mail varchar(254) not null,
     constraint IDUsers primary key (Username));

create table Payment (
     Value numeric(11,2) not null,
     Timestamp date not null,
     OwnerUsername varchar(15) not null,
     MovementStartTime date not null,
     constraint IDPayment primary key (Timestamp, OwnerUsername, MovementStartTime),
     foreign key (OwnerUsername, MovementStartTime) references Movement (Owner, Timestamp));

create table Movement (
     Owner varchar(15) not null,
     Timestamp date not null,
     Title varchar(50) not null,
     Value numeric(11,2) not null,
     CurrentValue numeric(11,2) not null,
     SubjectName varchar(30),
     Target varchar(15),
     Done BOOLEAN default 0 not null,
     constraint IDMovement primary key (Owner, Timestamp)
     foreign key (Target) references Users (Username),
     foreign key (Owner) references Users (Username));

CREATE UNIQUE INDEX UniqueUserMail ON Users (Mail);
CREATE INDEX TimestampMovementIndex ON Movement (Timestamp DESC);
CREATE INDEX TimestampPaymentIndex ON Payment (Timestamp DESC);
