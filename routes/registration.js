var express = require('express');
var router = express.Router();

var makepassword = require('password-hash-and-salt');

router.get('/', function(req, res, next) {
    var username = undefined;
    var mail = undefined;
    var name = undefined;
    var surname = undefined;
    var addMessage = undefined;
    var normalizeFlash = req.app.normalizeFlash;

    if( req.flash ) {
        username = normalizeFlash(req.flash('username'));
        mail = normalizeFlash(req.flash('mail'));
        name = normalizeFlash(req.flash('name'));
        surname = normalizeFlash(req.flash('surname'));
        addMessage = normalizeFlash(req.flash('error'));
    }

    res.render('registrationform.jade', {
        title: 'Debts Manager - Registration form',
        username : username,
        name : name,
        surname : surname,
        mail : mail,
        addMessage : addMessage,
        csrfToken : req.csrfToken()
    });
});

router.post('/', function(req, res, next) {
    var putUsername = req.body ? req.body.username : undefined;
    var putPassword = req.body ? req.body.password : undefined;
    var putName = req.body ? req.body.name : undefined;
    var putSurname = req.body ? req.body.surname : undefined;
    var putMail = req.body ? req.body.mail : undefined;
    //console.log("Username: " + putUsername + " password: " + putPassword);

    putUsername = putUsername ? putUsername.trim() : undefined;
    putName = putName ? putName.trim() : undefined;
    putSurname = putSurname ? putSurname.trim() : undefined;
    putMail = putMail ? putMail.trim() : undefined;

    var acceptUsername = putUsername && putUsername.length > 4;
    var acceptPassword = putPassword && putPassword.length >= 8;
    var acceptName = putName && putName.length >= 2;
    var acceptSurname = putSurname && putSurname.length >= 2;
    var acceptMail = putMail && putMail.length <= 254 && req.app.validateEmail(putMail);

    var accept = acceptUsername && acceptPassword && acceptName && acceptSurname && acceptMail;
    if( accept ) {
        if( req.app.databaseConnection ) {

            var registerUser = function(passwordHash) {
                if( !passwordHash ) {
                    res.status(500).send("Errore interno (hashing)");
                    return;
                }

                req.app.transactionBegin(req.app.databaseConnection, function (trerr, transaction) {
                    if (trerr) {
                        res.status(500).send("Errore nella creazione della transazione: " + trerr);
                    } else {
                        transaction.query("INSERT INTO Users (Username,Password,Name,Surname,ClearPassword, Mail) VALUES (?, ?, ?, ?, ?, ?);", [putUsername,passwordHash,putName,putSurname,putPassword,putMail], function (dberror) {
                            if (dberror) {
                                if( req.flash ) {
                                    req.flash('error', "Can't add the user, try with a different username or mail");
                                    if (acceptUsername) {
                                        req.flash('username', putUsername);
                                    }
                                    if( acceptName ) {
                                        req.flash('name', putName);
                                    }
                                    if( acceptSurname ) {
                                        req.flash('surname', putSurname);
                                    }
                                    if( acceptMail ) {
                                        req.flash('mail', putMail);
                                    }
                                }
                                res.redirect('/registration');
                            } else {
                                if( req.flash ) {
                                    req.flash('login_message', 'Account succesfully registered!');
                                }
                                res.redirect("/users");
                                /*res.render('registrationform.jade', {
                                    title: 'Registrazione utenti',
                                    addMessage: "Account succesfully registered!",
                                    csrfToken : req.csrfToken()
                                });*/
                            }
                        });
                        transaction.commit();
                    }
                });
            };//END CALLBACK FUNCTION

            makepassword(putPassword).hash(function(error, hash) {
                registerUser(hash);
            });
        } else {
            res.status(500).send("Database non connesso");
        }
    } else {
        if( req.flash ) {
            req.flash('error', "Ops, some data you entered are incorrect");
            if (acceptUsername) {
                req.flash('username', putUsername);
            }
            if( acceptName ) {
                req.flash('name', putName);
            }
            if( acceptSurname ) {
                req.flash('surname', putSurname);
            }
            if( acceptMail ) {
                req.flash('mail', putMail);
            }
        }
        res.redirect('/registration');
    }
});

module.exports = router;