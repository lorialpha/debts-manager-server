var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    req.logout();
    req.session.destroy(function (err) {
        if (err) {
            res.send("Impossibile eseguire il logout");
        } else {
            res.redirect("/");
        }
    });
});

module.exports = router;

