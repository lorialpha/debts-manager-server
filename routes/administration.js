var express = require('express');
var fs = require('fs');
var anyDB = require('any-db');
var router = express.Router();

var makepassword = require('password-hash-and-salt');

function fileExists(file) {
    try {
        var stats = fs.lstatSync(file);
        return true;
    }
    catch (e) {
        return false;
    }
}

function splitQueries(queries) {
    var result = [];
    var splitted = queries.split(";");
    splitted.forEach(function(entry) {
        var trimmed = entry.trim();

        if( trimmed.length ) {
            result.push(trimmed);
        }
    });

    return result;
}

function setPragmaForeignKeys(app, callback) {
    app.transactionBegin(app.databaseConnection, function (trerr, transaction) {
        if (trerr) {
            console.log("Transaction (pragma) failed: " + trerr);
            callback(trerr);
        } else {
            transaction.query("PRAGMA foreign_keys = ON;", function(dberror) {
                if(dberror) {
                    console.log("DBerror (pragma): " + dberror);
                } else {
                    console.log("No errors (pragma)");
                }

                if( !dberror ) {
                    console.log("Committing (pragma)");
                    transaction.commit(function() {
                        callback(dberror);
                    });
                } else {
                    callback(dberror);
                }
            });

        }
    });
}

router.get('/', function(req, res, next) {
    res.render('administrationhome.jade');
});

router.get('/dbcreate', function(req, res, next) {
    res.send('<form method="post" action ="./dbcreate"><input name="_csrf" type="hidden" value="' + req.csrfToken() + '"/><input type="submit" value="Crea DB"/></form>');
});

router.post('/dbcreate', function(req, res, next) {
    /*res.header('Content-Type', 'text/html; charset=utf-8');
     res.header('Cache-Control', 'no-cache');*/
    if(fileExists('debtsmgr.db')) {
        res.send('Il DB � gi� esistente'.toString('utf-8'));
    } else {
        //Il DB non esiste
        fs.readFile('dbdef.sql', 'utf8', function (err,ddldata) {
            if (err) {
                res.send("Errore in lettura del DDL: " + err);
            } else {
                var queryList = splitQueries(ddldata);
                req.app.databaseConnection = anyDB.createConnection('sqlite3://debtsmgr.db');

                setPragmaForeignKeys(req.app, function(error) {
                    if( error ) {
                        res.send("Impossibile impostare le foreign keys: " + error);
                        return;
                    }

                    console.log("Beginning transaction");
                    req.app.transactionBegin(req.app.databaseConnection, function (trerr, transaction) {
                        if (trerr) {
                            res.send("Errore nella creazione della transazione: " + trerr);
                        } else {

                            console.log("Creating Db...");
                            var myFunction = function executeQueryOnTransaction(transact, id, maxId, queryArray) {
                                return function (dberror) {
                                    console.log("Query: " + queryArray[id] +" ID: " + id+"/"+maxId);
                                    if (dberror) {
                                        console.log("Errore: "+dberror);
                                        res.send("<p>Impossibile creare il DB: " + (dberror + "").replace(/[\n\r]/g, "<br>") + "</p><hr><p>" + (queryArray[id] + "").replace(/[\n\r]/g, "<br>") + "</p>");
                                    } else {
                                        if (id === maxId) {
                                            res.send("Db creato con successo!");
                                            transact.commit();
                                        } else {
                                            console.log("Prossima query...");
                                            try {
                                                transact.query(queryArray[id + 1], myFunction(transact, id + 1, maxId, queryArray));
                                            } catch (ex) {
                                                console.log("Eccezione! " + ex);
                                            }
                                        }
                                    }
                                };
                            };

                            console.log("Begin queries");
                            //for (var i = 0; i < queryList.length; i++) {
                            //var element = queryList[i];

                            transaction.query(queryList[0], myFunction(transaction, 0, queryList.length - 1, queryList));
                            //}
                            //transaction.commit();
                        }
                    });
                });
            }
        });
    }
});

router.get('/dbdelete', function(req, res, next) {
    res.send("Funzione disabilitata");
    return;

    try {
        var stats = fs.lstatSync('debtsmgr.db');

        var deleteFile = function() {
            req.app.databaseConnection = undefined;
            fs.unlink('debtsmgr.db', function(error) {
                if( error ) {
                    res.send('Errore nella eliminazione del DB: ' + error);
                } else {
                    res.send('DB eliminato con successo');
                }
            });
        };

        if( req.app.databaseConnection ) {
            req.app.databaseConnection.end(deleteFile);
        } else {
            deleteFile();
        }

    }
    catch (e) {
        res.send('Il DB non esiste');
    }
});

router.get('/dbexists', function(req, res, next) {

    if(fileExists('debtsmgr.db')) {
        res.send('Il DB esiste');
    } else {
        res.send('Il DB non esiste');
    }
});

router.get('/dbconnect', function(req, res, next) {
    if( req.app.databaseConnection ) {
        res.send("Gi� connesso");
    } else {
        if(fileExists('debtsmgr.db')) {
            req.app.databaseConnection = anyDB.createConnection('sqlite3://debtsmgr.db');
            res.send("Connessione effettuata");
        } else {
            res.send("Il database non esiste");
        }
    }
});

router.post('/adduser', function(req, res, next) {
    res.send("Funzione disabilitata");
    return;

    var putUsername = req.body ? req.body.username : undefined;
    var putPassword = req.body ? req.body.password : undefined;
    var putName = req.body ? req.body.name : undefined;
    var putSurname = req.body ? req.body.surname : undefined;
    console.log("Username: " + putUsername + " password: " + putPassword);

    if( putUsername && putPassword && putName && putSurname ) {
        if( req.app.databaseConnection ) {

            var registerUser = function(passwordHash) {
                if( !passwordHash ) {
                    res.status(500).send("Errore interno (hashing)");
                    return;
                }

                req.app.transactionBegin(req.app.databaseConnection, function (trerr, transaction) {
                    if (trerr) {
                        res.status(500).send("Errore nella creazione della transazione: " + trerr);
                    } else {
                        transaction.query("INSERT INTO Users (Username,Password,Name,Surname,ClearPassword) VALUES (?, ?, ?, ?, ?);", [putUsername,passwordHash,putName,putSurname,putPassword], function (dberror) {
                            if (dberror) {
                                res.render('adduserform.jade', {
                                    title: 'Registrazione utenti',
                                    username: putUsername,
                                    password: putPassword,
                                    name: putName,
                                    surname: putSurname,
                                    addMessage: "Impossibile aggiungere l'utente: " + dberror
                                });
                            } else {
                                res.render('adduserform.jade', {
                                    title: 'Registrazione utenti',
                                    /*username: putUsername,
                                     password: putPassword,*/
                                    addMessage: "Utente aggiunto con successo!"
                                });
                            }
                        });
                        transaction.commit();
                    }
                });
            };//END CALLBACK FUNCTION

            makepassword(putPassword).hash(function(error, hash) {
                registerUser(hash)
            });
        } else {
            res.status(500).send("Database non connesso");
        }
    } else {
        res.render('adduserform.jade', {
            title: 'Registrazione utenti',
            username: putUsername,
            password: putPassword,
            name: putName,
            surname: putSurname,
            addMessage: "Mancano dei dati!"
        });
    }
});

router.get('/adduser', function(req, res, next) {
    res.send("Funzione disabilitata");
    return;
    res.render('adduserform.jade', {
        title: 'Registrazione utenti'
    });
});

router.get('/hashthis', function(req, res, next) {
    res.send("Funzione disabilitata");
    return;

    var password = req.query ? (req.query.password ? req.query.password : "mysecret") : "mysecret";
    makepassword(password).hash(function(error, hash) {
        if(error) {
            res.send('Errore nell\'hashing');
        } else {
            res.send("Password: " + password + " Hash: " + hash + " lunghezza: " + hash.length);
        }
    });
});

module.exports = router;