var express = require('express');
var waterfall = require('async-waterfall');
var router = express.Router();
var makepassword = require('password-hash-and-salt');
var MIN_VALUE = -999999999.00;
var MAX_VALUE = 999999999.00;

router.post('/users', function(req, res, next) {
    var app = req.app;
    var putUsername = req.body ? req.body.username : undefined;
    var putPassword = req.body ? req.body.password : undefined;
    var putName = req.body ? req.body.name : undefined;
    var putSurname = req.body ? req.body.surname : undefined;
    var putMail = req.body ? req.body.mail : undefined;

    putUsername = putUsername ? putUsername.trim() : undefined;
    putName = putName ? putName.trim() : undefined;
    putSurname = putSurname ? putSurname.trim() : undefined;
    putMail = putMail ? putMail.trim() : undefined;

    var acceptUsername = putUsername && putUsername.length > 4;
    var acceptPassword = putPassword && putPassword.length >= 8;
    var acceptName = putName && putName.length >= 2;
    var acceptSurname = putSurname && putSurname.length >= 2;
    var acceptMail = putMail && putMail.length <= 254 && req.app.validateEmail(putMail);

    var accept = acceptUsername && acceptPassword && acceptName && acceptSurname && acceptMail;
    if( accept ) {
        if( !app.databaseConnection ) {
            res.status(500).end();
            return;
        }

        var registerUser = function(passwordHash) {
            if (!passwordHash) {
                res.status(500).end();

            } else {
                waterfall([
                    app.transactionBeginWaterfall(),
                    function (transaction, callback) {
                        transaction.query("INSERT INTO Users (Username,Password,Name,Surname,ClearPassword, Mail) VALUES (?, ?, ?, ?, ?, ?);",
                            [putUsername, passwordHash, putName, putSurname, putPassword, putMail],
                            app.transactionCallbackWaterfallHelper(callback, transaction));
                    }, function (transaction, dbresult, callback) {
                        transaction.commit();
                        callback(null, true);
                    }
                ], function (err, result) {
                    if (err) {
                        res.status(409).end();
                    } else {
                        res.status(201).end();
                    }
                });
            }
        };

        makepassword(putPassword).hash(function(error, hash) {
            registerUser(hash);
        });
    } else {
        var errObj = {};
        errObj["username"] = acceptUsername;
        errObj["name"] = acceptName;
        errObj["surname"] = acceptSurname;
        errObj["mail"] = acceptMail;
        res.json(400).json(errObj);
    }
});

router.get('/user', function(req, res, next) {
    var username = req.user.username;
    var name = req.user.name;
    var surname = req.user.surname;

    res.status(200).json({surname: surname, name: name, username: username});
});

router.post('/logout', function(req, res, next) {
    req.logout();
    req.session.destroy(function (err) {
        if (err) {
            res.status(403).end();
        } else {
            res.status(200).end();
        }
    });
});

router.get('/movements/bytimestamp/:timestamp', function(req, res, next) {
    var app = req.app;
    var username = req.user.username;
    var movementTime = req.params ? req.params.timestamp : undefined;

    var acceptTime = false;
    if( movementTime ) {
        try {
            movementTime = new Date(movementTime);
            acceptTime = !isNaN(movementTime);
        } catch (e) {}
    }

    if( acceptTime) {
        var formattedTime = app.getTimeFromDate(movementTime);
        waterfall([
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    transaction.query("SELECT * FROM Movement WHERE Owner = ? AND Timestamp = ?",
                        [username, formattedTime], app.transactionCallbackWaterfallHelper(callback));

                    transaction.commit();
                },
                function(dbresult, callback) {
                    if( dbresult.rows.length == 0 ) {
                        callback("Not found");
                    } else {
                        callback(null, dbresult.rows[0]);
                    }

                }],
            function (err, result) {
                if(err) {
                    if( err == "Not found") {
                        res.status(404).end();
                    } else {
                        res.status(500).end();
                    }
                } else {
                    result.Done = (result.Done === "1" || result.Done === 1);

                    res.status(200).json(result);
                }
            }
        );
    } else {
        res.status(400).end();
    }
});

router.get('/movements/undone', function(req, res, next) {
    var app = req.app;
    var username = req.user.username;

    waterfall([
            app.transactionBeginWaterfall(),
            function(transaction, callback) {
                transaction.query("SELECT * FROM Movement WHERE Owner = ? AND NOT Done ORDER BY Timestamp DESC",
                    [username], app.transactionCallbackWaterfallHelper(callback));

                transaction.commit();
            },
            function(dbresult, callback) {
                callback(null, dbresult.rows);
            }],
        function (err, result) {
            if(err) {
                res.status(404).end();
            } else {
                for( var i = 0; i < result.length; i++ ) {
                    result[i].Done = (result[i].Done === "1" || result[i].Done === 1);
                }

                res.status(200).json(result);
            }
        }
    );
});

router.get('/movements/timestamps', function(req, res, next) {
    var app = req.app;
    var username = req.user.username;

    waterfall([
            app.transactionBeginWaterfall(),
            function(transaction, callback) {
                transaction.query("SELECT Timestamp,Done FROM Movement WHERE Owner = ? ORDER BY Timestamp DESC",
                    [username], app.transactionCallbackWaterfallHelper(callback));

                transaction.commit();
            },
            function(dbresult, callback) {
                callback(null, dbresult.rows);
            }],
        function (err, result) {
            if(err) {
                res.status(404).end();
            } else {
                res.status(200).json(result);
            }
        }
    );
});

router.post('/movements', function(req, res, next) {
    var app = req.app;
    var username = req.user.username;

    var putTitle = req.body ? req.body.title : undefined;
    var putValue = req.body ? req.body.value : undefined;
    var putSubject = req.body ? req.body.subject : undefined;
    var putDate = req.body ? req.body.date : undefined;

    putTitle = putTitle ? putTitle.trim() : undefined;
    putSubject = putSubject ? putSubject.trim() : undefined;
    putValue = putValue ? parseFloat(putValue) : undefined;
    putValue = (putValue && !isNaN(putValue)) ? putValue.toFixed(2) : undefined;
    putDate = putDate ? new Date(putDate) : undefined;

    var acceptTitle = putTitle && putTitle.length > 0;
    var acceptSubject = putSubject && putSubject.length > 0;

    var acceptValue = putValue && putValue != 0 && putValue > MIN_VALUE && putValue < MAX_VALUE;
    var acceptDate = putDate != undefined && !isNaN(putDate);
    var accept =  acceptTitle && acceptSubject && acceptValue && acceptDate;

    if( accept ) {
        //var formattedTime = app.getTimeNow();

        var findFirstFreeTimeSlot = function(transaction, timestamp, retryLimit, callback) {
            if( retryLimit < 1 ) {
                callback(false, true);
                return;
            }

            var dateStr = req.app.getTimeFromDate(timestamp);
            transaction.query("SELECT Timestamp FROM Movement WHERE Owner = ? AND Timestamp = ?",
                [username,dateStr], function(err, result) {
                    if( err ) {
                        callback(false, false);
                    } else {
                        if( result.rows.length == 0 ) {
                            callback(timestamp);
                        } else {
                            timestamp.setSeconds(timestamp.getSeconds() + 1);
                            findFirstFreeTimeSlot(transaction, timestamp, retryLimit-1, callback);
                        }
                    }
                } );
        };

        waterfall(
            [
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    var defensiveCopy = new Date(putDate.getTime());
                    findFirstFreeTimeSlot(transaction, defensiveCopy, 10, function(value, isConflict) {
                        if(value) {
                            callback(null, transaction, value);
                        } else {
                            if( isConflict ) {
                                transaction.commit();
                                callback("Conflict");
                            } else {
                                callback("Db error");
                            }
                        }
                    })
                },
                function(transaction, timestamp, callback) {
                    transaction.query("INSERT INTO Movement(Owner,Timestamp,Title,Value,CurrentValue,SubjectName,Target) " +
                        "VALUES (?,?,?,?,?,?,NULL)",
                        [username,req.app.getTimeFromDate(timestamp),putTitle, putValue, putValue, putSubject],
                        app.transactionCallbackWaterfallHelper(callback, {
                            transaction: transaction,
                            timestamp: timestamp
                        }));
                },
                function(bundle, dbresult, callback) {
                    bundle.transaction.commit();
                    callback(null, bundle.timestamp);
                }
            ], function (err, result) {
                if(err) {
                    if( err == "Conflict" ) {
                        res.status(409).end();
                    } else {
                        res.status(500).end();
                    }
                } else {
                    res.status(201).json({Timestamp: req.app.getTimeFromDate(result)});
                }
            }
        );
    } else {
        var errorObj = {};
        errorObj["title"] = acceptTitle;
        errorObj["subject"] = acceptSubject;
        errorObj["value"] = acceptValue;
        errorObj["date"] = acceptDate;

        console.log(errorObj);

        res.status(400).json(errorObj);
    }
});

router.get("/movements/:timestamp/payments", function(req, res, next) {
    var app = req.app;
    var username = req.user.username;

    var putTime = req.params ? req.params.timestamp : undefined;

    var acceptTime = false;
    if( putTime ) {
        try {
            putTime = new Date(putTime);
            acceptTime = true;
        } catch (e) {}
    }

    console.log(putTime);

    if( acceptTime ) {
        var formattedTime = app.getTimeFromDate(putTime);
        waterfall(
            [
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    transaction.query("SELECT * FROM Movement WHERE Owner = ? AND Timestamp = ?",
                        [username,formattedTime],app.transactionCallbackWaterfallHelper(callback));

                    transaction.commit();
                },
                function(dbresult, callback) {
                    if(dbresult.rows.length == 0) {
                        callback("Invalid movement timestamp");
                    } else {
                        callback(null);
                    }
                },
                app.transactionBeginWaterfall(app),
                function(transaction, callback) {
                    transaction.query("SELECT Value,Timestamp FROM Payment WHERE OwnerUsername = ? AND MovementStartTime = ? " +
                        "ORDER BY Timestamp DESC", [username,formattedTime],app.transactionCallbackWaterfallHelper(callback));

                    transaction.commit();
                },
                function(dbresult, callback) {
                    callback(null, dbresult.rows);
                }
            ], function (err, result) {
                if(err) {
                    if( err == "Invalid movement timestamp" ) {
                        res.status(404).end();
                    } else {
                        res.status(500).end();
                    }
                } else {
                    res.status(200).json(result);
                }
            }
        );
    } else {
        res.status(400).end();
    }
});

router.post("/movements/:timestamp/payments", function(req, res, next) {
    var app = req.app;
    var username = req.user.username;

    var putMovementTime = req.params ? req.params.timestamp : undefined;
    var putPaymentTime = req.body ? req.body.paymentTimestamp : undefined;
    var putValue = req.body ? req.body.value : undefined;
    putValue = putValue != undefined ? parseFloat(putValue) : undefined;
    putValue = (putValue != undefined && !isNaN(putValue)) ? putValue.toFixed(2) : undefined;
    putMovementTime = putMovementTime ? new Date(putMovementTime) : undefined;
    putPaymentTime = putPaymentTime ? new Date(putPaymentTime) : undefined;

    var acceptMovementTime = putMovementTime && !isNaN(putMovementTime);
    var acceptPaymentTime = putPaymentTime && !isNaN(putPaymentTime);
    var acceptValue = putValue != undefined && putValue != 0 && putValue > MIN_VALUE && putValue < MAX_VALUE;

    var accept = acceptMovementTime && acceptPaymentTime && acceptValue;

    if( accept ) {
        var movementDateStr = req.app.getTimeFromDate(putMovementTime);
        var findFirstFreeTimeSlot = function(transaction, timestamp, retryLimit, callback) {
            if( retryLimit < 1 ) {
                callback(false, true);
                return;
            }

            var dateStr = req.app.getTimeFromDate(timestamp);
            transaction.query("SELECT Timestamp FROM Payment WHERE OwnerUsername = ? AND Timestamp = ? AND MovementStartTime = ?",
                [username,dateStr,movementDateStr], function(err, result) {
                    if( err ) {
                        callback(false, false);
                    } else {
                        if( result.rows.length == 0 ) {
                            callback(timestamp);
                        } else {
                            timestamp.setSeconds(timestamp.getSeconds() + 1);
                            findFirstFreeTimeSlot(transaction, timestamp, retryLimit-1, callback);
                        }
                    }
                } );
        };

        waterfall(
            [
                app.transactionBeginWaterfall(),
                function (transaction, callback) {
                    transaction.query("SELECT Timestamp FROM Movement WHERE Owner = ? AND Timestamp = ?",
                        [username, movementDateStr], app.transactionCallbackWaterfallHelper(callback, transaction));
                },
                function(transaction, dbresult, callback) {
                    if( dbresult.rows.length === 0 ) {
                        transaction.commit();
                        callback("Not found");
                    } else {
                        callback(null, transaction);
                    }
                },
                function(transaction, callback) {
                    var defensiveCopy = new Date(putPaymentTime.getTime());
                    findFirstFreeTimeSlot(transaction, defensiveCopy, 10, function(value, isConflict) {
                        if(value) {
                            callback(null, transaction, value);
                        } else {
                            if( isConflict ) {
                                transaction.commit();
                                callback("Conflict");
                            } else {
                                callback("Db error");
                            }
                        }
                    })
                },
                function(transaction, paymentTimestamp, callback) {
                    transaction.query("INSERT INTO Payment(OwnerUsername,MovementStartTime,Timestamp,Value) " +
                        "VALUES (?,?,?,?)",
                        [username,movementDateStr,req.app.getTimeFromDate(paymentTimestamp), putValue],
                        app.transactionCallbackWaterfallHelper(callback, {
                            transaction: transaction,
                            paymentTimestamp : paymentTimestamp
                        }));
                },
                function(bundle, dbresult, callback) {
                    bundle.transaction.query("UPDATE Movement SET CurrentValue = CurrentValue + ? WHERE Owner = ? AND Timestamp = ?",
                        [putValue, username, movementDateStr], app.transactionCallbackWaterfallHelper(callback, bundle));
                },
                function(bundle, dbresult, callback) {
                    bundle.transaction.commit();
                    callback(null, bundle.paymentTimestamp);
                }
            ], function (err, result) {
                if (err) {
                    if( err == "Conflict" ) {
                        res.status(409).end();
                    } else if( err == "Not found" ) {
                        res.status(404).end();
                    } else {
                        console.log(err);
                        res.status(500).end();
                    }
                } else {
                    res.status(201).json({Timestamp: req.app.getTimeFromDate(result)});
                }
            }
        );
    } else {
        res.status(400).end();
    }
});

router.post('/movements/:timestamp/done/', function(req, res, next) {
    var app = req.app;
    var putTime = req.params ? req.params.timestamp : undefined;
    var putDone = req.body ? req.body.done : undefined;

    var username = req.user.username;
    var acceptTime = false;
    var acceptDone = putDone === "1" || putDone === "0" ||
        putDone === "true" || putDone === "false" ||
        putDone === 1 || putDone === 0 || putDone === true || putDone === false;

    if( putTime ) {
        try {
            putTime = new Date(putTime);
            acceptTime = !isNaN(putTime);
        } catch (e) {}
    }

    var formattedTime;
    if( acceptTime ) {
        formattedTime = app.getTimeFromDate(putTime);
    }

    if( acceptTime && acceptDone ) {

        if( putDone === "true" || putDone === "1" || putDone === true ) {
            putDone = 1;
        } else {
            putDone = 0;
        }

        waterfall([
                app.transactionBeginWaterfall(),
                function (transaction, callback) {
                    transaction.query("SELECT Timestamp FROM Movement WHERE Owner = ? AND Timestamp = ?",
                        [username, formattedTime], app.transactionCallbackWaterfallHelper(callback, transaction));
                },
                function(transaction, dbresult, callback) {
                    if( dbresult.rows.length === 0 ) {
                        transaction.commit();
                        callback("Not found");
                    } else {
                        callback(null, transaction);
                    }
                },
                function (transaction, callback) {
                    transaction.query("UPDATE Movement SET DONE = ? WHERE Owner = ? AND Timestamp = ?",
                        [putDone, username, formattedTime], app.transactionCallbackWaterfallHelper(callback, transaction));
                },
                function (transaction, dbresult, callback) {
                    transaction.commit();
                    callback(null, dbresult.rows);
                }],
            function (err, result) {
                if (err) {
                    if( err == "Not found" ) {
                        res.status(404).end();
                    } else {
                        res.status(500).end();
                    }
                } else {
                    res.status(200).end();
                }
            }
        );
    } else {
        res.status(400).end();
    }
});

router.get('/movements/done', function(req, res, next) {
    var app = req.app;
    var username = req.user.username;
    waterfall([
            app.transactionBeginWaterfall(),
            function(transaction, callback) {
                transaction.query("SELECT * FROM Movement WHERE Owner = ? AND Done ORDER BY Timestamp DESC",
                    [username], app.transactionCallbackWaterfallHelper(callback));

                transaction.commit();
            },
            function(dbresult, callback) {
                callback(null, dbresult.rows);
            }],
        function (err, result) {
            if(err) {
                res.status(500).end();
            } else {
                for( var i = 0; i < result.length; i++ ) {
                    result[i].Done = (result[i].Done === "1" || result[i].Done === 1);
                }

                res.status(200).json(result);
            }
        }
    );
});

module.exports = router;
