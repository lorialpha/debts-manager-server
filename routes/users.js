var express = require('express');
var waterfall = require('async-waterfall');
var router = express.Router();
var MIN_VALUE = -999999999.00;
var MAX_VALUE = 999999999.00;

router.get('/', function(req, res, next) {
    var loginMessage = "";
    if( req.flash ) {
        var flashMessage = req.app.normalizeFlash(req.flash('error'));
        if( flashMessage ) {
            loginMessage = flashMessage;
        } else {
            flashMessage = req.app.normalizeFlash(req.flash('login_message'));
            if( flashMessage ) {
                loginMessage = flashMessage;
            }
        }
    }

    res.render('loginform.jade', {
        loginMessage : loginMessage,
        title : 'Login',
        csrfToken : req.csrfToken()
    });
});

router.get('/personal', function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return next ? next() : 0;
    }

    var app = req.app;
    var username = req.user.username;
    var user_name = req.user.name;
    var user_surname = req.user.surname;
    waterfall([
            app.transactionBeginWaterfall(),
        function( transaction, callback) {
            transaction.query("SELECT * FROM Movement WHERE Owner = ? AND NOT Done ORDER BY Timestamp DESC",
                [username], app.transactionCallbackWaterfallHelper(callback));

            transaction.commit();
        },
        function(dbresult, callback) {
            callback(null, dbresult.rows);
        }],
        function (err, result) {
            if(err) {
                req.flash('error', ""+err);
                res.redirect('./');
            } else {
                res.render('personalpage.jade', {
                    title : 'Personal page',
                    csrfToken : req.csrfToken(),
                    user_name : user_name,
                    user_surname : user_surname,
                    username : username,
                    movements: result
                });
            }
        }
    );
});

router.get('/add', function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return next ? next() : 0;
    }

    if( req.flash ) {
        var addMessage = undefined;
        var mov_value = undefined;
        var mov_title = undefined;
        var mov_subject = undefined;
        addMessage = req.flash('error');
        mov_value = req.flash('value');
        mov_title = req.flash('title');
        mov_subject = req.flash('subject');

        addMessage = addMessage.length ? addMessage : undefined;
        mov_value = mov_value.length ? mov_value : undefined;
        mov_title = mov_title.length ? mov_title : undefined;
        mov_subject = mov_subject.length ? mov_subject : undefined;

        res.render('addmovementform.jade', {
            title : 'Add movement',
            addMessage : addMessage,
            csrfToken : req.csrfToken(),
            mov_value : mov_value,
            mov_title : mov_title,
            mov_subject : mov_subject,
            user_name : req.user.name,
            user_surname : req.user.surname,
            username : req.user.username
        });
    } else {
        res.render('addmovementform.jade', {
            title : 'Add movement',
            csrfToken : req.csrfToken()
        });
    }
});

router.post('/add', function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return next ? next() : 0;
    }

    var app = req.app;
    var username = req.user.username;

    var putTitle = req.body ? req.body.title : undefined;
    var putValue = req.body ? req.body.value : undefined;
    var putSubject = req.body ? req.body.subject : undefined;

    putTitle = putTitle ? putTitle.trim() : undefined;
    putSubject = putSubject ? putSubject.trim() : undefined;
    putValue = putValue ? parseFloat(putValue) : undefined;
    putValue = (putValue && !isNaN(putValue)) ? putValue.toFixed(2) : undefined;

    var acceptTitle = putTitle && putTitle.length > 0;
    var acceptSubject = putSubject && putSubject.length > 0;
    var acceptValue = putValue && putValue != 0 && putValue > MIN_VALUE && putValue < MAX_VALUE;
    var accept =  acceptTitle && acceptSubject && acceptValue;

    if( accept ) {
        var formattedTime = app.getTimeNow();
        console.log(formattedTime);
        waterfall(
            [
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    transaction.query("INSERT INTO Movement(Owner,Timestamp,Title,Value,CurrentValue,SubjectName,Target) " +
                        "VALUES (?,?,?,?,?,?,NULL)",
                        [username,formattedTime,putTitle, putValue, putValue, putSubject], app.transactionCallbackWaterfallHelper(callback));

                    transaction.commit();
                },
                function(dbresult, callback) {
                    callback(null, dbresult.rows);
                }
            ], function (err, result) {
                if(err) {
                    if( req.flash ) {
                        req.flash('error', "Invalid input data");
                    }
                    res.redirect('/users/add');
                } else {
                    req.flash('addMessage', "Movement added succesfully");
                    res.redirect('/users/personal');
                }
            }
        );
    } else {
        if( req.flash ) {
            var errorMsg = "Invalid input data. ";
            if( !acceptTitle ) {
                errorMsg += "Invalid title.";
            } else {
                req.flash('title', putTitle);
            }
            if( !acceptSubject ) {
                errorMsg += "Invalid friend name.";
            } else {
                req.flash('subject', putSubject);
            }
            if( !acceptValue ) {
                errorMsg += "Invalid value.";
            } else {
                req.flash('value', putValue);
            }
            req.flash('error', errorMsg);
        }

        res.redirect('/users/add');
    }
});

router.get("/edit", function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return;
    }

    var app = req.app;
    var username = req.user.username;

    var putTime = req.query ? req.query.time : undefined;

    var acceptTime = false;
    if( putTime ) {
        try {
            putTime = new Date(putTime);
            acceptTime = true;
        } catch (e) {}
    }

    if( acceptTime ) {
        var formattedTime = app.getTimeFromDate(putTime);
        waterfall(
            [
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    transaction.query("SELECT * FROM Movement WHERE Owner = ? AND Timestamp = ?",
                        [username,formattedTime],app.transactionCallbackWaterfallHelper(callback));

                    transaction.commit();
                },
                function(dbresult, callback) {
                    if(dbresult.rows.length == 0) {
                        callback("Invalid movement timestamp");
                    } else {
                        callback(null, dbresult.rows[0]);
                    }
                },
                app.transactionBeginWaterfall(),
                function(movement, transaction, callback) {
                    transaction.query("SELECT Value,Timestamp FROM Payment WHERE OwnerUsername = ? AND MovementStartTime = ? " +
                    "ORDER BY Timestamp DESC", [username,formattedTime],app.transactionCallbackWaterfallHelper(callback, movement));

                    transaction.commit();
                },
                function(movement, dbresult, callback) {
                    callback(null, {
                        movement: movement,
                        payments: dbresult.rows
                    });
                }
            ], function (err, result) {
                if( err ) {
                    res.redirect("/users/personal");
                } else {
                    var value = undefined;
                    var editMessage = undefined;
                    if(req.flash) {
                        value = req.app.normalizeFlash(req.flash('value'));
                        editMessage = req.app.normalizeFlash(req.flash('error'));
                    }
                    res.render('movementedit.jade', {
                        username : username,
                        user_name : req.user.name,
                        user_surname : req.user.surname,
                        value : value,
                        movement: result.movement,
                        payments: result.payments,
                        editMessage : editMessage,
                        csrfToken : req.csrfToken()
                    });
                }
            }
        );
    } else {
        res.redirect("/users/personal");
    }
});

router.post("/edit/do", function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return;
    }

    var app = req.app;
    var username = req.user.username;

    var putTime = req.body ? req.body.time : undefined;
    var putValue = req.body ? req.body.value : undefined;

    var acceptTime = false;
    var acceptValue = putValue && putValue != 0 && putValue > MIN_VALUE && putValue < MAX_VALUE;

    if( putTime ) {
        try {
            putTime = new Date(putTime);
            acceptTime = true;
        } catch (e) {}
    }

    var formattedTime;
    if( acceptTime ) {
        formattedTime = app.getTimeFromDate(putTime);
    }

    if( acceptTime && acceptValue ) {
        var formattedTimeNow = app.getTimeNow();
        waterfall(
            [
                app.transactionBeginWaterfall(),
                function(transaction, callback) {
                    transaction.query("INSERT INTO Payment(OwnerUsername,MovementStartTime,Timestamp,Value) " +
                        "VALUES (?,?,?,?)",
                        [username,formattedTime,formattedTimeNow, putValue], app.transactionCallbackWaterfallHelper(callback, transaction));
                },
                function(transaction, dbresult, callback) {
                    transaction.query("UPDATE Movement SET CurrentValue = CurrentValue + ? WHERE Owner = ? AND Timestamp = ?",
                        [putValue, username, formattedTime], app.transactionCallbackWaterfallHelper(callback, transaction));
                },
                function(transaction, dbresult, callback) {
                    transaction.commit();
                    callback(null);
                }
            ], function (err, result) {
                if(err) {
                    if( req.flash ) {
                        req.flash('error', "Can't add payment :(. Try again");
                    }
                    res.redirect("/users/edit?time="+formattedTime);
                } else {
                    res.redirect("/users/personal");
                }
            }
        );
    } else {
        if( acceptTime ) {
            if( req.flash ) {
                req.flash('error', "Invalid money value!");
            }
            res.redirect("/users/edit?time="+formattedTime);
        } else {
            res.redirect("/users/personal");
        }
    }
});

router.get('/done', function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return next ? next() : 0;
    }

    var app = req.app;
    var username = req.user.username;
    waterfall([
            app.transactionBeginWaterfall(),
            function(transaction, callback) {
                transaction.query("SELECT * FROM Movement WHERE Owner = ? AND Done ORDER BY Timestamp DESC",
                    [username], app.transactionCallbackWaterfallHelper(callback));

                transaction.commit();
            },
            function(dbresult, callback) {
                callback(null, dbresult.rows);
            }],
        function (err, result) {
            if(err) {
                req.flash('error', ""+err);
                res.redirect('./');
            } else {
                res.render('donepage.jade', {
                    title : 'Done movements',
                    csrfToken : req.csrfToken(),
                    user_name : req.user.name,
                    user_surname : req.user.surname,
                    username : username,
                    movements: result
                });
            }
        }
    );
});

router.post('/setdone', function(req, res, next) {
    if( !req.isAuthenticated() ) {
        res.redirect('./');
        return next ? next() : 0;
    }

    var app = req.app;
    var putTime = req.body ? req.body.time : undefined;
    var putDone = req.body ? req.body.done : undefined;

    var username = req.user.username;
    var acceptTime = false;
    var acceptDone = putDone === "1" || putDone === "0";

    if( putTime ) {
        try {
            putTime = new Date(putTime);
            acceptTime = true;
        } catch (e) {}
    }

    var formattedTime;
    if( acceptTime ) {
        formattedTime = app.getTimeFromDate(putTime);
    }

    if( acceptTime && acceptDone ) {
        if( putDone === "1" ) {
            putDone = 1;
        } else {
            putDone = 0;
        }

        waterfall([
                app.transactionBeginWaterfall(),
                function (transaction, callback) {
                    transaction.query("UPDATE Movement SET DONE = ? WHERE Owner = ? AND Timestamp = ?",
                        [putDone, username, formattedTime], app.transactionCallbackWaterfallHelper(callback, transaction));

                },
                function (transaction, dbresult, callback) {
                    transaction.commit();
                    callback(null, dbresult.rows);
                }],
            function (err, result) {
                if (err) {
                    if( req.flash ) {
                        req.flash('error', "" + err);
                    }
                    res.redirect("/users/personal");
                } else {
                    if( putDone === 1 ) {
                        res.redirect("/users/personal");
                    } else {
                        res.redirect("/users/done");
                    }

                }
            }
        );
    } else {
        if( req.flash ) {
            req.flash('error', "Can't find the movement");
        }
        res.redirect("/users/personal");
    }
});

module.exports = router;
