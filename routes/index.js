var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    if( req.isAuthenticated() ) {
        res.redirect('./users');
    } else {
        res.render('index.jade', {
            title: 'A simple cloud debts manager',
            csrfToken : req.csrfToken()
        });
    }
});

module.exports = router;
