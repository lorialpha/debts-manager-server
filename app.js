var express = require('express');//project default
var path = require('path');//project default
var favicon = require('serve-favicon');//project default
var logger = require('morgan');//project default
var cookieParser = require('cookie-parser');//project default
var bodyParser = require('body-parser');//project default
var csurf = require('csurf');
var session = require('express-session');
var memoryStore = session.MemoryStore();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var flash = require('connect-flash');
var makepassword = require('password-hash-and-salt');
var jsesc = require('jsesc');

var routes = require('./routes/index');//project default
var users = require('./routes/users');//project default
var accountlogin = require('./routes/accountlogin');
var accountlogout = require('./routes/accountlogout');
var administration = require('./routes/administration');
var registration = require('./routes/registration');
var rest = require('./routes/rest');

var app = express();//project default

var anyDB = require('any-db');


var databaseConnection;
var fs = require('fs');
try {
    var dbStats = fs.lstatSync('debtsmgr.db');

    databaseConnection = anyDB.createConnection('sqlite3://debtsmgr.db');
    console.log("database connected!" );
}
catch (e) {
    console.log("exception " + e );

    databaseConnection = undefined;
}

var transactionBegin = require('any-db-transaction');

// view engine setup
app.set('views', path.join(__dirname, 'views'));//project default
app.set('view engine', 'jade');//project default

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));//project default
app.use(bodyParser.json());//project default
app.use(bodyParser.urlencoded({ extended: false }));//project default
app.use(cookieParser());//project default
app.use(session({
    store: memoryStore,
    secret: "PRf90UsbI1lGwiRg2MAv2GbVtv06lkBIv72LlXL3L5qSXfZphVxI7GbtNetgCr0I3y1BDcLhY2kF1pBACUiFICODN4iauZnGPX7L9OPHw22yATV7oAQ10jbumNyueOVOXoqIpfKq1aGHSBz4GElAvadTEMc4VWLxuMtbaQMgTvr3g96n4O1LTsku8u7s0IpVaLRguxHI",
    resave: false,
    saveUninitialized: false,
    unset: 'destroy'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


passport.use('local',new LocalStrategy(
    function(username, password, done) {
        if( app.databaseConnection ) {

            app.transactionBegin(app.databaseConnection, function (trerr, transaction) {
                if (trerr) {
                    done(null, false, { message: "Errore nella creazione della transazione: " + trerr });
                } else {
                    var successCallback = function(name, surname) {
                        done(null, {username: username, name : name, surname : surname});
                    };

                    var failCallback = function() {
                        done(null, false, {message: "Credenziali errate"});
                    };

                    transaction.query("SELECT Name,Surname,Password FROM Users WHERE Username = ?", [username], function (dberror, dbresult) {
                        if (!(dberror || dbresult.rowCount <= 0)) {
                            var passwordHash = dbresult.rows[0]["Password"];
                            makepassword(password).verifyAgainst(passwordHash, function (hasherror, hashverified) {
                                if (hashverified && !hasherror) {
                                    successCallback(dbresult.rows[0]["Name"], dbresult.rows[0]["Surname"]);
                                } else {
                                    failCallback();
                                }
                            });
                        } else{
                            failCallback()
                        }
                    });

                    transaction.commit();
                }
            });
        } else {
            done(null, false, { message: "Database non connesso"});
        }
    }
));

passport.use('basic', new BasicStrategy(
    function(username, password, done) {
        if( app.databaseConnection ) {

            app.transactionBegin(app.databaseConnection, function (trerr, transaction) {
                if (trerr) {
                    done(null, false, { message: "Errore nella creazione della transazione: " + trerr });
                } else {
                    var successCallback = function(name, surname) {
                        done(null, {username: username, name : name, surname : surname});
                    };

                    var failCallback = function() {
                        done(null, false, {message: "Credenziali errate"});
                    };

                    transaction.query("SELECT Name,Surname,Password FROM Users WHERE Username = ?", [username], function (dberror, dbresult) {
                        if (!(dberror || dbresult.rowCount <= 0)) {
                            var passwordHash = dbresult.rows[0]["Password"];
                            makepassword(password).verifyAgainst(passwordHash, function (hasherror, hashverified) {
                                if (hashverified && !hasherror) {
                                    successCallback(dbresult.rows[0]["Name"], dbresult.rows[0]["Surname"]);
                                } else {
                                    failCallback();
                                }
                            });
                        } else{
                            failCallback()
                        }
                    });

                    transaction.commit();
                }
            });
        } else {
            done(null, false, { message: "Database non connesso"});
        }
    }
));

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

app.use(express.static(path.join(__dirname, 'public')));//project default

var csrfProtection = csurf();

/*
var defaultStringsRenderMiddleware = function(req, res, next) {
    if( res.render ) {
        var prevRender = res.render;
        res.render = function(view, locals, callback) {
            if(!locals) {
                locals = {};
            } else if(typeof locals === 'function') {
                callback = locals;
                locals = {};
            }

            locals["def"] = {
                apptitle: "Debts Manager"
            };
            res.render = prevRender;

            res.render(view, locals, callback);
        };
    }

    if(next) {
        next();
    }
};*/
/*
var jsescAddMiddleware = function(req, res, next) {
    res.locals["jsesc"] = jsesc;
    if(next) {
        next();
    }
};*/

//app.use(defaultStringsRenderMiddleware);
//app.use(jsescAddMiddleware);

app.use(function(req,res,next) {
    var acceptType = req.get('Accept');
    var isAcceptJson = acceptType && acceptType.match( /^application\/json;?/ );
    var contentType = req.get('Content-Type');
    var isContentJson = contentType && contentType.match( /^application\/json;?/ );
    if ( isContentJson && req.path === "/rest/users" ) {
        next();
    } else if( (isAcceptJson || isContentJson) && req.get('Authorization')) {
        passport.authenticate('basic', { session: false })(req,res,next);
    } else {
        csrfProtection(req,res,next);
    }
});
//app.use();
app.use('/', routes);//project default
app.use('/users', users);
app.use('/registration', registration);
app.use('/administration', administration);
app.use('/rest', function(req,res,next) {
    if( req.isAuthenticated() || req.path === "/users" ) {
        rest(req,res,next);
    } else {
        res.status(403).end();
    }
});

app.use('/users/login', passport.authenticate('local', { successRedirect: '/users/personal',
    failureRedirect: '/users',
    failureFlash: true,
    badRequestMessage: 'Username o password non forniti'}), accountlogin);

app.use('/users/logout', accountlogout);
app.use('/layouttest', function(req, res, next) {
    res.render("emptytest.jade");
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

/*app.projectConfiguration = {
    personalPageItems: 5
};*/
app.databaseConnection = databaseConnection;
app.transactionBegin = transactionBegin;
app.jsesc = jsesc;
app.locals["def"] = {apptitle: "Debts Manager"};
app.locals["jsesc"] = jsesc;
app.locals["time_human_format_no_sec"] = function (timestamp) {
    var date = new Date(timestamp);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    //var seconds = "0" + date.getSeconds();
    return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes.substr(-2); //+ ':' + seconds.substr(-2)
};

app.normalizeFlash = function(flashValue) {
    if( flashValue && flashValue.length ) {
        return flashValue;
    } else {
        return undefined;
    }
};

app.validateEmail = function(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

app.transactionBeginWaterfall = function () {
    return function( data, callback ) {
        if( data && !callback ) {
            callback = data;
            data = undefined;
        }
        if (app.databaseConnection) {
            app.transactionBegin(app.databaseConnection, function (err, transaction) {
                if (err) {
                    callback(err);
                } else {
                    if( data ) {
                        callback(null, data, transaction);
                    } else {
                        callback(null, transaction);
                    }

                }
            });
        } else {
            callback("Database not connected");
        }
    };
};

app.transactionCallbackWaterfallHelper = function( callback, data ) {
    return function( err, dbresult ) {
        if( err ) {
            callback(err);
        } else {
            if( data ) {
                callback( null, data, dbresult );
            } else {
                callback( null, dbresult );
            }
        }
    };
};

app.getTimeFromDate = function(date) {
   /* var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);*/
    return date.toISOString();
};

app.getTimeNow = function() {
    var date = new Date();
    return app.getTimeFromDate(date);
};

module.exports = app;
